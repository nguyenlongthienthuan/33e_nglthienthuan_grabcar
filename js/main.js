const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";

function tinhGiaTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 10000;
  }
}
function tinhGiaTiemKm1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
    default:
      return 0;
  }
}
function tinhGiaTiemKm19TroDi(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
    default:
      return 0;
  }
}
// main function
function tinhTienUber() {
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
 
  var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
  console.log("giaTienKmDauTien: ", giaTienKmDauTien);

  var giaTienKm1_19 = tinhGiaTiemKm1_19(carOption);
  console.log("giaTienKm1_19: ", giaTienKm1_19);

  var giaTienKm19TroDi = tinhGiaTiemKm19TroDi(carOption);
  console.log("giaTienKm19TroDi: ", giaTienKm19TroDi);
  var thanhtien_id=document.getElementById("xuatTien");
  var div_thanhtien=document.getElementById("divThanhTien");
  var input_value=document.getElementById("txt-km").value * 1;
  if (input_value <=1){
    thanhtien_id.innerHTML =( giaTienKmDauTien * input_value) ;
   div_thanhtien.style.display="block";
   console.log("yes");}
  else if (input_value>1 && input_value<=19){
    thanhtien_id.innerHTML =( giaTienKmDauTien + (input_value-1)* giaTienKm1_19) ;
    div_thanhtien.style.display="block";
    console.log("yes");
   }else if(input_value >19){
    thanhtien_id.innerHTML =( giaTienKmDauTien +18 * giaTienKm1_19 +( input_value-19)* giaTienKm19TroDi) ;
    div_thanhtien.style.display="block";
    console.log("yes");
   }
  
}
